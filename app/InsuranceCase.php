<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceCase extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'case_title', 'brand_id', 'brand_model_id', 'mileage', 'buying_date', 'color_id', 'drivetrain', 'picture', 'user_id', 'is_finished'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
