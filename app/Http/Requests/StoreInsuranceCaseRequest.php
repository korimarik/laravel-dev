<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInsuranceCaseRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'case_title' => 'nullable|string|max:250',
            'brand_id' => 'nullable|exists:brands,id',
            'brand_model_id' => 'nullable|exists:brand_models,id',
            'mileage' => 'nullable|string|max:250',
            'buying_date' => 'nullable|date',
            'color_id' => 'nullable|exists:colors,id',
            'drivetrain' => 'nullable|string|max:250',
            'picture' => 'nullable|string|max:250'
        ];
    }
}
