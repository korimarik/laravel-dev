<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;

class ColorController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getColors()
    {
        $colors = Color::all();

        return response()->json($colors);
    }
}
