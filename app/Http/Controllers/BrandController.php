<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;

class BrandController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBrands()
    {
        $brands = Brand::all();
        return response()->json($brands);
    }

    /**
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBrandModels(Brand $brand) {

        return response()->json($brand->brandModels);
    }
}
