<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BrandModel;


class BrandModelController extends Controller
{

    /**
     * @param BrandModel $brandModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function view(BrandModel $brandModel)
    {

        return response()->json($brandModel);
    }
}
