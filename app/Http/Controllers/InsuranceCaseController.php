<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInsuranceCaseRequest;
use App\InsuranceCase;

class InsuranceCaseController extends Controller
{

    /**
     * @param StoreInsuranceCaseRequest $request
     * @param InsuranceCase $insuranceCase
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StoreInsuranceCaseRequest $request, InsuranceCase $insuranceCase)
    {
        $insuranceCase->update($request->all());

        return response()->json('OK', 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIncuranceCases()
    {
        $authUser = \Auth::user();
        $insuranceCases = $authUser->insuranceCases()->where('is_finished', true)->get();

        return response()->json($insuranceCases);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotFinishedInsuranceCase()
    {
        $authUser = \Auth::user();
        $insuranceCase = $authUser->insuranceCase()->where('is_finished', false)->first();

        return response()->json($insuranceCase);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        $authUser = \Auth::user();
        $authUser->insuranceCase()->create();
        $insuranceCase = $authUser->insuranceCase()->where('is_finished', false)->first();

        return response()->json($insuranceCase, 201);
    }
}
