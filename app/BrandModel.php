<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
