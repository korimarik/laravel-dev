# laravel-dev-test

Test project for the Laravel position

### 1. Close the repository
```$xslt
git clone https://gitlab.com/korimarik/laravel-dev.git
```
### 2. Cd to the project directory
```angular2html
cd laravel-dev

```
### 3. Install Composer dependencies
```$xslt
composer install
```

### 4. Add writable permissions for two directories
```angular2html
chmod 777 -R storage/ bootstrap/cache
```

### 5. Create a MySQL database(e.g. `laravel-dev`)

### 6. Copy `.env.example` to `.env` file in the root directory.
```angular2html
cp .env.example .env
```

### 7. Adjust the following settings:
```$xslt
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<database_name>
DB_USERNAME=<username_of_your_server>
DB_PASSWORD=<password_of_your_server>
```

### 8. Migrate tables of the Database
```$xslt
php artisan migrate
```

### 9. Seed the Database
```$xslt
php artisan db:seed
```

### 10. Generate application key
```$xslt
php artisan key:generate
```

### 11. Start backend server
```$xslt
php artisan serve
```

### 12. Open the following url
```$xslt
http://127.0.0.1:8000/
```

### 13. Login credentials:
login: admin@example.com

password: password
