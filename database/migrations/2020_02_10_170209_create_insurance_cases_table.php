<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_title', 255)->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('brand_model_id')->nullable();
            $table->unsignedInteger('mileage')->nullable();
            $table->date('buying_date')->nullable();
            $table->unsignedBigInteger('color_id')->nullable();
            $table->string('drivetrain', 255)->nullable();
            $table->string('picture', 255)->nullable();
            $table->boolean('is_finished')->default(false);
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('brand_id')
                ->references('id')->on('brands');
            $table->foreign('brand_model_id')
                ->references('id')->on('brand_models');
            $table->foreign('color_id')
                ->references('id')->on('colors');
            $table->foreign('user_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_cases');
    }
}
