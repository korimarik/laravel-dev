<?php

use Illuminate\Database\Seeder;
use App\Brand;
use App\BrandModel;
use Illuminate\Support\Facades\DB;

class BrandsBrandModelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $count = Brand::count();
        if ($count == 0) {


            $brands = [
                'BMW', 'Mercedes', 'Jeep'
            ];

            $brandModels = [
                'BMW' => [
                    ['model_name' => '3 Series', 'is_drivetrainable' => false],
                    ['model_name' => '5 Series', 'is_drivetrainable' => false],
                    ['model_name' => '7 Series', 'is_drivetrainable' => false]
                ],
                'Mercedes' => [
                    ['model_name' => 'C Class', 'is_drivetrainable' => false],
                    ['model_name' => 'E Class', 'is_drivetrainable' => false],
                    ['model_name' => 'S Class', 'is_drivetrainable' => false]
                ],
                'Jeep' => [
                    ['model_name' => 'Wrangler', 'is_drivetrainable' => false],
                    ['model_name' => 'Cherokee', 'is_drivetrainable' => false],
                    ['model_name' => 'Grand Cherokee', 'is_drivetrainable' => true]
                ]
            ];

            foreach ($brands as $brand) {
                $brandObj = Brand::create([
                    'brand_name' => $brand
                ]);
                foreach ($brandModels[$brand] as $model) {
                    $brandObj->brandModels()->save(new BrandModel($model));
                }
            }
        }
    }
}
