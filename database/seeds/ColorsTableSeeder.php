<?php

use Illuminate\Database\Seeder;
use App\Color;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = Color::count();
        if ($count == 0) {

            Color::insert([
                ['color_name' => 'white'],
                ['color_name' => 'silver'],
                ['color_name' => 'black'],
                ['color_name' => 'other'],
            ]);
        }
    }
}
