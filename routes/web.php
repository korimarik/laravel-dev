<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::group(['middleware' => 'auth:web'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/brands', 'BrandController@getBrands');
    Route::get('/brands-get-models/{brand}', 'BrandController@getBrandModels');
    Route::get('/insurance-case', 'InsuranceCaseController@getIncuranceCases');
    Route::get('/insurance-case-not-finished', 'InsuranceCaseController@getNotFinishedInsuranceCase');
    Route::post('/insurance-case', 'InsuranceCaseController@store');
    Route::put('/insurance-case/{insuranceCase}', 'InsuranceCaseController@update');

    Route::get('/colors', 'ColorController@getColors');

    Route::get('/brand-models/{brandModel}', 'BrandModelController@view');

});
