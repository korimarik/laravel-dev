import axios from 'axios';

export default class Color {

    /**
     *
     * @returns {{id: null, color_title: null}}
     */
    static getSample() {
        return {
            id: null,
            color_title: null
        }
    };

    /**
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    static getColors() {
        return axios
            .get('/colors/');
    }

}
