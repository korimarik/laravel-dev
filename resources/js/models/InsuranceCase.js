import axios from 'axios';

export default class InsuranceCase {

    /**
     *
     * @returns {{case_title: null, brand_id: null, brand_model_id: null, mileage: null, bying_date: null, color_id: null, drivetrain: null, picture: null, is_finished: boolean}}
     */
    static getSample() {
        return {
            case_title: null,
            brand_id: null,
            brand_model_id: null,
            mileage: null,
            bying_date: null,
            color_id: null,
            drivetrain: null,
            picture: null,
            is_finished: false
        }
    };

    /**
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    static getBrands() {
        return axios
            .get('/brands/');
    }

    /**
     *
     * @param brandId
     * @returns {Promise<AxiosResponse<T>>}
     */
    static getBrandModels(brandId) {
        return axios
            .get('/brands-get-models/' + brandId);
    }

    /**
     *
     * @param insuranceCaseId
     * @param data
     * @returns {Promise<AxiosResponse<T>>}
     */
    static updateFieldValue(insuranceCaseId, data) {

        return axios
            .put('/insurance-case/' + insuranceCaseId, data);
    }

    /**
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    static getInsuranceCases() {

        return axios
            .get('/insurance-case');
    }

    /**
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    static getNotFinishedInsuranceCase() {

        return axios
            .get('/insurance-case-not-finished');
    }

    /**
     *
     * @returns {Promise<AxiosResponse<T>>}
     */
    static createNew() {
        return axios.post('/insurance-case');
    }
}
