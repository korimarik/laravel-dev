import axios from 'axios';

export default class BrandModel {

    /**
     *
     * @param modelId
     * @returns {Promise<AxiosResponse<T>>}
     */
    static getModelById(modelId) {
        return axios
            .get('/brand-models/' + modelId);
    }

}
