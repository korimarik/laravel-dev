import Vue from 'vue';

export const eventBus = new Vue({
    data() {
        return {
            isButtonDeactivated: false,
            isDrivetrainable: false
        };
    },
    methods: {
        setIsButtonDeactivated(value) {
            this.isButtonDeactivated = value;
        },
        getIsButtonDeactivated() {
            return this.isButtonDeactivated;
        },
        setIsDrivetrainable(value) {
            this.isDrivetrainable = value;
        },
        getIsDrivetrainable() {
            return this.isDrivetrainable;
        },

    },
});
